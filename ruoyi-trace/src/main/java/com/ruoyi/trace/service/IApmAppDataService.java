package com.ruoyi.trace.service;

import com.ruoyi.trace.domain.ApmAppData;

import java.util.List;

/**
 * 链路Service接口
 * 
 * @author ruoyi
 * @date 2021-07-23
 */
public interface IApmAppDataService 
{
    /**
     * 查询链路
     * 
     * @param dataId 链路ID
     * @return 链路
     */
    public ApmAppData selectApmAppDataById(Long dataId);

    /**
     * 查询链路列表
     * 
     * @param apmAppData 链路
     * @return 链路集合
     */
    public List<ApmAppData> selectApmAppDataList(ApmAppData apmAppData);

    /**
     * 新增链路
     * 
     * @param apmAppData 链路
     * @return 结果
     */
    public int insertApmAppData(ApmAppData apmAppData);

    /**
     * 修改链路
     * 
     * @param apmAppData 链路
     * @return 结果
     */
    public int updateApmAppData(ApmAppData apmAppData);

    /**
     * 批量删除链路
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteApmAppDataByIds(String ids);

    /**
     * 删除链路信息
     * 
     * @param dataId 链路ID
     * @return 结果
     */
    public int deleteApmAppDataById(Long dataId);
}
