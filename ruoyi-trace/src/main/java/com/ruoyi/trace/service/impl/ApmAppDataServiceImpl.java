package com.ruoyi.trace.service.impl;

import com.ruoyi.common.core.text.Convert;
import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.trace.domain.ApmAppData;
import com.ruoyi.trace.mapper.ApmAppDataMapper;
import com.ruoyi.trace.service.IApmAppDataService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 链路Service业务层处理
 * 
 * @author ruoyi
 * @date 2021-07-23
 */
@Service
public class ApmAppDataServiceImpl implements IApmAppDataService 
{
    @Autowired
    private ApmAppDataMapper apmAppDataMapper;

    /**
     * 查询链路
     * 
     * @param dataId 链路ID
     * @return 链路
     */
    @Override
    public ApmAppData selectApmAppDataById(Long dataId)
    {
        return apmAppDataMapper.selectApmAppDataById(dataId);
    }

    /**
     * 查询链路列表
     * 
     * @param apmAppData 链路
     * @return 链路
     */
    @Override
    public List<ApmAppData> selectApmAppDataList(ApmAppData apmAppData)
    {
        return apmAppDataMapper.selectApmAppDataList(apmAppData);
    }

    /**
     * 新增链路
     * 
     * @param apmAppData 链路
     * @return 结果
     */
    @Override
    public int insertApmAppData(ApmAppData apmAppData)
    {
        apmAppData.setCreateTime(DateUtils.getNowDate());
        return apmAppDataMapper.insertApmAppData(apmAppData);
    }

    /**
     * 修改链路
     * 
     * @param apmAppData 链路
     * @return 结果
     */
    @Override
    public int updateApmAppData(ApmAppData apmAppData)
    {
        return apmAppDataMapper.updateApmAppData(apmAppData);
    }

    /**
     * 删除链路对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteApmAppDataByIds(String ids)
    {
        return apmAppDataMapper.deleteApmAppDataByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除链路信息
     * 
     * @param dataId 链路ID
     * @return 结果
     */
    @Override
    public int deleteApmAppDataById(Long dataId)
    {
        return apmAppDataMapper.deleteApmAppDataById(dataId);
    }
}
