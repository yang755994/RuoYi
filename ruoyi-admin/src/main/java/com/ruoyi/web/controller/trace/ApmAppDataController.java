package com.ruoyi.web.controller.trace;

import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.trace.domain.ApmAppData;
import com.ruoyi.trace.service.IApmAppDataService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 链路Controller
 * 
 * @author ruoyi
 * @date 2021-07-23
 */
@Controller
@RequestMapping("/trace/data")
public class ApmAppDataController extends BaseController
{
    private String prefix = "trace/data";

    @Autowired
    private IApmAppDataService apmAppDataService;

    @RequiresPermissions("trace:data:view")
    @GetMapping()
    public String data()
    {
        return prefix + "/data";
    }

    /**
     * 查询链路列表
     */
    @RequiresPermissions("trace:data:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(ApmAppData apmAppData)
    {
        startPage();
        List<ApmAppData> list = apmAppDataService.selectApmAppDataList(apmAppData);
        return getDataTable(list);
    }

    /**
     * 导出链路列表
     */
    @RequiresPermissions("trace:data:export")
    @Log(title = "链路", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(ApmAppData apmAppData)
    {
        List<ApmAppData> list = apmAppDataService.selectApmAppDataList(apmAppData);
        ExcelUtil<ApmAppData> util = new ExcelUtil<ApmAppData>(ApmAppData.class);
        return util.exportExcel(list, "链路数据");
    }

    /**
     * 新增链路
     */
    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }

    /**
     * 新增保存链路
     */
    @RequiresPermissions("trace:data:add")
    @Log(title = "链路", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(ApmAppData apmAppData)
    {
        return toAjax(apmAppDataService.insertApmAppData(apmAppData));
    }

    /**
     * 修改链路
     */
    @GetMapping("/edit/{dataId}")
    public String edit(@PathVariable("dataId") Long dataId, ModelMap mmap)
    {
        ApmAppData apmAppData = apmAppDataService.selectApmAppDataById(dataId);
        mmap.put("apmAppData", apmAppData);
        return prefix + "/edit";
    }

    /**
     * 修改保存链路
     */
    @RequiresPermissions("trace:data:edit")
    @Log(title = "链路", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(ApmAppData apmAppData)
    {
        return toAjax(apmAppDataService.updateApmAppData(apmAppData));
    }

    /**
     * 删除链路
     */
    @RequiresPermissions("trace:data:remove")
    @Log(title = "链路", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(apmAppDataService.deleteApmAppDataByIds(ids));
    }
}
